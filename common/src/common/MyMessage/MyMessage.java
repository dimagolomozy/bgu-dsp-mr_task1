package common.MyMessage;

public abstract class MyMessage
{
    protected final String type;
    protected final String workId;
    protected final String clientId;

    // Protocol specific constants
    public static final String messageNewTask = "<NEWTASK>";
    public static final String messageTaskCompleted = "<TASKCOMPLETED>";
    public static final String messageTerminate = "<TERMINATE>";
    public static final String messageWork = "<WORK>";
    public static final String messageWorkResult = "<RESULT>";
    protected static final String delimiter = ";";

    protected MyMessage(String type, String workId, String clientId)
    {
        this.type = type;
        this.workId = workId;
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }

    public String getType()
    {
        return type;
    }

    public String getWorkId()
    {
        return workId;
    }

    public abstract String toString();

    public static MyMessage getMessage(String message) throws UnsupportedOperationException
    {
        String[] msg = message.split(delimiter);
        switch (msg[0])
        {
            case messageWork:
                return new MyMessageWork(msg[1], msg[2], msg[3], msg[4]);
            case messageWorkResult:
                return new MyMessageWorkResult(msg[1], msg[2], msg[3], msg[4], Boolean.valueOf(msg[5]));
            case messageTaskCompleted:
                return new MyMessageTaskCompleted(msg[1], msg[2], msg[3], msg[4]);
            case messageTerminate:
                return new MyMessageTerminate(msg[1], msg[2], msg[3], Boolean.valueOf(msg[4]));
            case messageNewTask:
                return new MyMessageNewTask(msg[1], msg[2], msg[3], Integer.valueOf(msg[4]), msg[5]);
            default:
                throw new UnsupportedOperationException(String.format("Could not parse message: %s" ,message));
        }
    }
}
