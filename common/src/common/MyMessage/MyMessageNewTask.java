package common.MyMessage;


/**
 * Represents a New Task for a Manager
 */
public class MyMessageNewTask extends MyMessage
{
    private final String path;  // S3 Path for inputFile
    private final String returnQueue;  // Queue to send results to
    private final int messagesPerWorker;  // amount of "messages" per worker

    public MyMessageNewTask(String workId, String clientId, String path, int messagesPerWorker, String returnQueue)
    {
        super(messageNewTask, workId, clientId);
        this.path = path;
        this.messagesPerWorker = messagesPerWorker;
        this.returnQueue = returnQueue;
    }

    @Override
    public String toString()
    {
        return "" +
                type               + delimiter +
                workId             + delimiter +
                clientId           + delimiter +
                path               + delimiter +
                messagesPerWorker  + delimiter +
                returnQueue;
    }

    public String getPath()
    {
        return path;
    }

    public String getReturnQueue() { return returnQueue; }

    public int getMessagesPerWorker() {
        return messagesPerWorker;
    }
}
