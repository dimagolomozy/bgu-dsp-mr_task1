package common.MyMessage;


/**
 * Represents a new Work for a worker
 */
public class MyMessageWork extends MyMessage
{
    private final String path;  // Path for inputFile (the image)
    private final String returnQueue;  // Queue to send results to (the response with the results)

    public MyMessageWork(String workId, String clientId, String path, String returnQueue)
    {
        super(messageWork, workId, clientId);
        this.path = path;
        this.returnQueue = returnQueue;
    }

    @Override
    public String toString()
    {
        return "" +
                type        + delimiter +
                workId      + delimiter +
                clientId    + delimiter +
                path        + delimiter +
                returnQueue;
    }

    public String getPath()
    {
        return path;
    }

    public String getReturnQueue() { return returnQueue; }
}
