package common.MyMessage;


public class MyMessageTerminate extends MyMessage
{
    private final boolean isWorker;
    private final String returnQueue;
    public MyMessageTerminate(String terminateId, String clientId, String returnQueue, boolean isWorker)
    {
        super(messageTerminate, terminateId, clientId);
        this.isWorker = isWorker;
        this.returnQueue = returnQueue;
    }

    @Override
    public String toString()
    {
        return "" +
                type        + delimiter +
                workId      + delimiter +
                clientId    + delimiter +
                returnQueue + delimiter +
                isWorker;
    }

    public boolean isWorker() {
        return isWorker;
    }

    public String getReturnQueue() {
        return returnQueue;
    }
}
