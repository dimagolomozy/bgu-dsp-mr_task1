package common.MyMessage;

/**
 * A task processing was completed (response message to client)
 */
public class MyMessageTaskCompleted extends MyMessage
{
    private final String originalInputFilePath;
    private final String resultsFile;

    public MyMessageTaskCompleted(String workId, String clientId, String originalInputFilePath, String resultsFile)
    {
        super(messageTaskCompleted, workId, clientId);
        this.originalInputFilePath = originalInputFilePath;
        this.resultsFile = resultsFile;
    }

    public String getOriginalInputFilePath()
    {
        return originalInputFilePath;
    }

    public String getResultsFile()
    {
        return resultsFile;
    }

    @Override
    public String toString()
    {
        return "" +
                type        + delimiter +
                workId      + delimiter +
                clientId    + delimiter +
                originalInputFilePath + delimiter +
                resultsFile;
    }
}
