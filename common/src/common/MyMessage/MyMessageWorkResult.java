package common.MyMessage;

/**
 * Represents a result of worker work. (e.g. an image that was processed)
 */
public class MyMessageWorkResult extends MyMessage
{
    private final String oldPath;
    private final String newPath;
    private final Boolean success;  // is the evaluation completed successfully or not

    public MyMessageWorkResult(String workId, String clientId, String oldPath, String newPath, Boolean success)
    {
        super(messageWorkResult, workId, clientId);
        this.oldPath = oldPath;
        this.newPath = newPath;
        this.success = success;
    }

    public String getOldPath()
    {
        return oldPath;
    }

    public String getNewPath()
    {
        return newPath;
    }

    public Boolean getSuccess() {
        return success;
    }

    @Override
    public String toString()
    {
        return "" +
                type        + delimiter +
                workId      + delimiter +
                clientId    + delimiter +
                oldPath     + delimiter +
                newPath     + delimiter +
                success;
    }
}
