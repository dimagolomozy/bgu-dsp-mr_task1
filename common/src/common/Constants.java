package common;

import java.io.InputStream;
import java.util.UUID;

public final class Constants {
    // AMAZON Constants
	public final static String awsCredentials = "AwsCredentials.properties";
    public final static InputStream awsCredentialInputStream = Constants.class.getResourceAsStream(awsCredentials);
    public final static String sshKeyName = "AWS_Eyal_Dima";
    public final static String managerRoleTag = "manager";
    public final static String workerRoleTag = "worker";

    // Workers Constants
    public final static double imageScale = 50;

    // Queue Names
    public final static String queueManagerInput = "manager-input-queue-123456789";
    public final static String queueWorkersInput = "workers-input-queue-123456789";
    public final static String queueClientInput = "client_input_queue" + UUID.randomUUID();

    // Bucket Names
    public final static String bucketName = "dima-eyal-bucket-123456789";
    public final static String bucketInputFolder = "inputFiles/";
    public final static String bucketOutputFolder = "outputFiles/";

    // Binaries
    public final static String binaryManager = "s3://binaries-bucket/Manager.jar";
    public final static String binaryWorker = "s3://binaries-bucket/Worker.jar";

    // TODO: XXXXXXXXXXX
    // boolean for starting instances or we are working local
    public final static boolean isLocal = false;

}
