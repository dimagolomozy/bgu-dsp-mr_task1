package common.Wrappers;

import Logger.MyLogger;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.*;
import com.amazonaws.util.Base64;
import common.Constants;

import java.util.ArrayList;
import java.util.List;

public class EC2Wrapper
{
    private final AmazonEC2 ec2;

    public EC2Wrapper(final AWSCredentials awsCredentials)
    {
        ec2 = new AmazonEC2Client(awsCredentials);
    }

    /**
     * Launches the specified number of instances using an AMI for which you
     * have permissions.
     * @param instanceAmount The amount of instances to create
     * @param keyName The SSH key we want to use for remote management;
     * @return One or more instances.
     * @throws AmazonClientException
     *             If any internal errors are encountered inside the client while
     *             attempting to make the request or handle the response.  For example
     *             if a network connection is not available.
     * @throws AmazonServiceException
     *             If an error response is returned by AmazonEC2 indicating
     *             either a problem with the data in the request, or a server side issue.
     */
    public List<Instance> createInstances(final int instanceAmount, final String keyName, final String bootScript)
            throws AmazonClientException
    {
        RunInstancesRequest request = new RunInstancesRequest();
        request.withImageId("ami-146e2a7c")
                .withInstanceType("t2.micro")
                .withSecurityGroups("launch-wizard-4")
                .withMinCount(instanceAmount)
                .withMaxCount(instanceAmount)
                .withKeyName(keyName);

        if (bootScript != null) {
            String encoded = new String(Base64.encode(bootScript.getBytes()));
            request.withUserData(encoded);
        }

        // Load created instances
        return ec2.runInstances(request).getReservation().getInstances();
    }

    public List<Instance> createInstances(final int instanceAmount, final String keyName) {
        return createInstances(instanceAmount, keyName, null);
    }

    /**
     * Attaches a tag to a list of instances
     * @param instances
     * @param tagValue
     * @throws AmazonClientException
     */
    public void tagInstances(List<Instance> instances, String tagValue)
            throws AmazonClientException
    {
        if (instances.size() == 0)
            return;

        CreateTagsRequest tagRequest = new CreateTagsRequest();
        for (Instance instance : instances) {
            tagRequest.withResources(instance.getInstanceId())
                      .withTags(new Tag("role", tagValue));
        }
        ec2.createTags(tagRequest);
    }

    public void terminateInstances(List<Instance> instances) {
        List<String> instancesIds = new ArrayList<String>();
        for (Instance instance : instances)
            instancesIds.add(instance.getInstanceId());

        ec2.terminateInstances(new TerminateInstancesRequest(instancesIds));
    }

    /**
     * Return the tag string value of an instance
     * @param instance
     * @return
     */
    public String getInstanceTag(Instance instance) {
        for (Tag tag : instance.getTags())
        {
            if (!tag.getKey().equals("role"))
                continue;

            return tag.getValue();
        }
        return "";
    }

    /**
     * Return RUNNING instances filtered by tag
     * @param tag
     * @return
     */
    public List<Instance> getInstancesByTag(String tag) {
        List<Reservation> reservations = getReservationsByKey("role");

        List<Instance> instances = new ArrayList<Instance>();

        for (Reservation reservation : reservations)
        {
            // For each reserved instance we can get multiple instances - check all of them
            for (Instance instance : reservation.getInstances())
            {
                if (instance.getState().getName().equals("terminated"))
                    continue;

                for (Tag itag : instance.getTags())
                {
                    // Ignore keys that are not 'role' (the only key we use)
                    if (!itag.getKey().equals("role"))
                        continue;

                    if (itag.getValue().equals(tag))
                        instances.add(instance);
                }
            }
        }
        return instances;
    }

    public List<Instance> getAllRunningInstances()
    {
        DescribeInstancesRequest request = new DescribeInstancesRequest();
        DescribeInstancesResult result = ec2.describeInstances(request);
        List<Reservation> reservations = result.getReservations();
        List<Instance> instances = new com.amazonaws.internal.ListWithAutoConstructFlag<>();
        for (Reservation reservation : reservations)
            instances.addAll(reservation.getInstances());

        return instances;
    }


    private List<Reservation> getReservationsByKey(final String key)
    {
        DescribeInstancesRequest request = new DescribeInstancesRequest();

        List<String> listOfTags = new ArrayList<>();
        listOfTags.add(key);

        Filter filter = new Filter("tag-key", listOfTags);
        request.withFilters(filter);

        // Get reserved instances
        return ec2.describeInstances(request).getReservations();
    }


}
