package common.Wrappers;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;

import java.io.File;

public class S3Wrapper
{
    private final AmazonS3 s3;

    /**
     * Constructor of S3Wrapper
     * @param awsCredentials Amazon Web Services credentials
     */
    public S3Wrapper (final AWSCredentials awsCredentials)
    {
        s3 = new AmazonS3Client(awsCredentials);
    }

    /**
     * Uploads file to bucket in S3
     * PRIVATE BY DEFAULT
     * @param bucketName The bucket name
     * @param fileName The file name
     * @param file The file
     * @return The file url in the bucket
     * @throws AmazonClientException
     *             If any errors are encountered in the client while making the
     *             request or handling the response.
     * @throws AmazonServiceException
     *             If any errors occurred in Amazon S3 while processing the
     *             request.
     */
    public String uploadFileToBucket(final String bucketName, final String fileName, final File file)
            throws AmazonClientException {
        return uploadFileToBucket(bucketName, fileName, file, false);
    }

    /**
     * Uploads file to bucket in S3
     * @param bucketName The bucket name
     * @param fileName The file name
     * @param file The file
     * @return The file url in the bucket
     * @throws AmazonClientException
     *             If any errors are encountered in the client while making the
     *             request or handling the response.
     * @throws AmazonServiceException
     *             If any errors occurred in Amazon S3 while processing the
     *             request.
     */
    public String uploadFileToBucket(final String bucketName, final String fileName, final File file, Boolean makePublic)
            throws AmazonClientException {
        PutObjectRequest req = new PutObjectRequest(bucketName, fileName, file);
        if (makePublic) {
            req.withCannedAcl(CannedAccessControlList.PublicRead);
        }
        s3.putObject(req);
        return fileName;
    }


    /**
     * Gets the object stored in Amazon S3 under the specified bucket and filePath
     * @param bucketName The bucket name
     * @param fileName The file path in S3
     * @return The object stored in Amazon S3 in the specified bucket and key.
     * @throws AmazonClientException
     *             If any errors are encountered in the client while making the
     *             request or handling the response.
     * @throws AmazonServiceException
     *             If any errors occurred in Amazon S3 while processing the
     *             request.
     */
    public S3Object getObjectFromBucket(final String bucketName, final String fileName)
            throws AmazonClientException
    {
        return s3.getObject(new GetObjectRequest(bucketName, fileName));
    }


    /**
     * Deletes the specified object in the specified bucket.
     * @param bucketName
     *            The name of the Amazon S3 bucket containing the object to
     *            delete.
     * @param fileName
     *            The key of the object to delete.
     *
     * @throws AmazonClientException
     *             If any errors are encountered in the client while making the
     *             request or handling the response.
     * @throws AmazonServiceException
     *             If any errors occurred in Amazon S3 while processing the
     *             request.
     */
    public void deleteFileFromBucket(final String bucketName, final String fileName)
            throws AmazonClientException
    {
        s3.deleteObject(bucketName, fileName);
    }


    /**
     * Creates a new Amazon S3 bucket with the specified name
     * @param bucketName
     *            The name of the bucket to create.
     *            All buckets in Amazon S3 share a single namespace;
     *            ensure the bucket is given a unique name.
     *
     * @return The newly created bucket.
     *
     * @throws AmazonClientException
     *             If any errors are encountered in the client while making the
     *             request or handling the response.
     * @throws AmazonServiceException
     *             If any errors occurred in Amazon S3 while processing the
     *             request.
     */
    public Bucket createBucket(final String bucketName)
            throws AmazonClientException
    {
        // checks if there is a bucket with the given name in my account. if so returns it.
        for (Bucket bucket : s3.listBuckets())
            if (bucketName.equals(bucket.getName()))
                return bucket;

        return s3.createBucket(bucketName);
    }

    /**
     * Deletes the specified bucket. All objects (and all object versions, if versioning
     * was ever enabled) in the bucket must be deleted before the bucket itself
     * can be deleted.

     * @param bucketName
     *            The name of the bucket to delete.
     *
     * @throws AmazonClientException
     *             If any errors are encountered in the client while making the
     *             request or handling the response.
     * @throws AmazonServiceException
     *             If any errors occurred in Amazon S3 while processing the
     *             request.
     */
    public void deleteBucket(final String bucketName)
            throws AmazonClientException
    {
        s3.deleteBucket(bucketName);
    }
}
