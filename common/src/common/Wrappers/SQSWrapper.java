package common.Wrappers;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;
import common.MyMessage.MyMessage;

import java.util.List;


public class SQSWrapper
{
    private final AmazonSQS sqs;

    /**
     * Constructor of SQSWrapper
     * @param awsCredentials Amazon Web Services credentials
     */
    public SQSWrapper(final AWSCredentials awsCredentials)
    {
        sqs = new AmazonSQSClient(awsCredentials);
    }


    /**
     * Create a queue with the given name.
     * @param queueName The name of the queue to create.
     * @return The URL for the created Amazon SQS queue.
     * @throws QueueNameExistsException
     * @throws QueueDeletedRecentlyException
     */
    public String createQueue(final String queueName)
            throws AmazonClientException
    {
        // If queue already exists - AWS API do nothing
        return sqs.createQueue(queueName).getQueueUrl();
    }

    /**
     * Delete the queue
     * @param queueUrl The queue url
     */
    public void deleteQueue(final String queueUrl)
            throws AmazonClientException
    {
        sqs.deleteQueue(queueUrl);
    }


    /**
     * Sends a given MyMessage to a given Queue
     * @param queueUrl the name of the queue
     * @param myMessage the common.MyMessage to send
     * @throws AmazonClientException
     *             If any internal errors are encountered inside the client while
     *             attempting to make the request or handle the response.  For example
     *             if a network connection is not available.
     * @throws AmazonServiceException
     *             If an error response is returned by AmazonSQS indicating
     *             either a problem with the data in the request, or a server side issue.
     */
    public void sendMessageToQueue(final String queueUrl, final MyMessage myMessage)
            throws AmazonClientException
    {
        sendMessageToQueue(queueUrl, myMessage.toString());
    }

    public void sendMessageToQueue(final String queueUrl, final String message)
            throws AmazonClientException
    {
        sqs.sendMessage(new SendMessageRequest(queueUrl, message));
    }

    /**
     * Deletes a given amazon message from the given queue
     * @param queueUrl the name of the queue
     * @param message the Amazon message to delete from the queue
     * @throws AmazonClientException
     *             If any internal errors are encountered inside the client while
     *             attempting to make the request or handle the response.  For example
     *             if a network connection is not available.
     * @throws AmazonServiceException
     *             If an error response is returned by AmazonSQS indicating
     *             either a problem with the data in the request, or a server side issue.
     */
    public void deleteMessageFromQueue(final String queueUrl, final Message message)
            throws AmazonClientException
    {
        String messageRecieptHandle = message.getReceiptHandle();
        sqs.deleteMessage(new DeleteMessageRequest(queueUrl, messageRecieptHandle));
    }

    /**
     * Receives Amazon messages from given queue url
     * @param queueUrl The queue url
     * @param maxMessages maximum messages to receive
     * @return A list of messages.
     * @throws AmazonServiceException
     * @throws AmazonClientException
     */
    public List<Message> receiveMessageFromQueue(final String queueUrl, final int maxMessages)
            throws AmazonClientException
    {
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl);
        receiveMessageRequest.setMaxNumberOfMessages(maxMessages);
        return sqs.receiveMessage(receiveMessageRequest).getMessages();
    }

    /**
     * The URL for the queue.
     * @param queueName The queue name.
     * @return The URL for the queue.
     * @throws AmazonClientException
     *             If any internal errors are encountered inside the client while
     *             attempting to make the request or handle the response.  For example
     *             if a network connection is not available.
     * @throws AmazonServiceException
     *             If an error response is returned by AmazonSQS indicating
     *             either a problem with the data in the request, or a server side issue.
     */
    public String getQueueUrl(final String queueName)
            throws AmazonClientException
    {
        return sqs.getQueueUrl(queueName).getQueueUrl();
    }


    public static String getAmazonClientException(AmazonClientException ace)
    {
        StringBuilder error = new StringBuilder();
        error.append("Caught an AmazonClientException, which means the client encountered "
                + "a serious internal problem while trying to communicate with SQS, "
                + "such as not being able to access the network.");
        error.append("Error Message: " + ace.getMessage());
        return error.toString();
    }

    public static String getAmazonServiceException(AmazonServiceException ase)
    {
        StringBuilder error = new StringBuilder();
        error.append("Caught an AmazonServiceException, which means your request made it " +
                "to Amazon SQS, but was rejected with an error response for some reason.");
        error.append("Error Message:    " + ase.getMessage());
        error.append("HTTP Status Code: " + ase.getStatusCode());
        error.append("AWS Error Code:   " + ase.getErrorCode());
        error.append("Error Type:       " + ase.getErrorType());
        error.append("Request ID:       " + ase.getRequestId());
        return error.toString();
    }

}
