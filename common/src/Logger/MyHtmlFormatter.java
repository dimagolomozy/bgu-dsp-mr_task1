package Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * 
 * custom HTML formatter
 *
 */

public class MyHtmlFormatter extends Formatter {
	
	@Override
	 public String format(LogRecord rec) {
	    StringBuffer buf = new StringBuffer(1000);
	    // Bold any levels >= WARNING
	    buf.append("<tr>");
	    buf.append("<td width = 200>");

	    if (rec.getLevel().intValue() >= Level.WARNING.intValue()) {
	      buf.append("<b>");
	      buf.append(rec.getLevel());
	      buf.append("</b>");
	    } else {
	      buf.append(rec.getLevel());
	    }
	    buf.append("</td>");
	    buf.append("<td width = 150>");
	    buf.append(calcDate(rec.getMillis()));
	    buf.append("</td>");
	    buf.append("<td>");
	    buf.append(formatMessage(rec).replaceAll("\n", "<br>"));
	    buf.append("</td>");
	    buf.append("</tr>");
	    return buf.toString();
	  }
	
	 // This method is called just after the handler using this
	  // formatter is created
	  public String getHead(Handler h) {
	    return "<HTML>\n<HEAD>\n" + (new Date()) 
	        + "\n</HEAD>\n<BODY>\n<PRE>\n"
	        + "<table width=\"100%\" border>\n  "
	        + "<tr><th>Level</th>" +
	        "<th>Time</th>" +
	        "<th>Log Message</th>" +
	        "</tr>\n";
	  }

	  // This method is called just after the handler using this
	  // formatter is closed
	  public String getTail(Handler h) {
	    return "</table>\n  </PRE></BODY>\n</HTML>\n";
	  }


	  private String calcDate(long millisecs) {
	    SimpleDateFormat date_format = new SimpleDateFormat("HH:mm:ss");
	    Date resultdate = new Date(millisecs);
	    return date_format.format(resultdate);
	  }


}
