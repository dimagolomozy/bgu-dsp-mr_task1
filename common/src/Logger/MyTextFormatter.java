package Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;


/**
 *  
 * Formatter class for custom formatting of Logger messages
 *  
 * @superHero BATMAN!!!
 *
 */

public class MyTextFormatter extends Formatter {
	
	
	@Override
	public String format(LogRecord rec) {
		StringBuilder str = new StringBuilder();
		
		str.append(calcDate(rec.getMillis()) + " | ");
		if (MyLogger.DEBUG) {
			str.append("Class: " + rec.getSourceClassName() + " Method: " + rec.getSourceMethodName());
			str.append("\r\n");
		}
		str.append("<" + rec.getLevel() + "> ");
		str.append(formatMessage(rec));
		str.append("\r\n");
		
		return str.toString();
	}
	
	/**
	 * calculate date and time
	 * @param millisecs
	 * @return date
	 */
	private String calcDate(long millisecs) {
	    SimpleDateFormat date_format = new SimpleDateFormat("HH:mm:ss");
	    Date resultdate = new Date(millisecs);
	    return date_format.format(resultdate);
	}
	
}
