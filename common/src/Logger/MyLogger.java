package Logger;

import java.io.File;
import java.util.logging.*;

/**
 *  
 * Logger class for logging messages during simulation.
 *
 */

public class MyLogger {
	public final static Logger LOGGER = Logger.getLogger(MyLogger.class.getName()); 
	public static boolean DEBUG = true;
	
	/**
	 * initialize Logger, set fileHandler and logging level
	 */
	public static boolean init(boolean debug, boolean console, boolean txt, boolean html) {
		try {
			new File("./Logger").mkdir();
			
			LOGGER.setLevel(Level.CONFIG);
			LOGGER.setUseParentHandlers(false);
			DEBUG = debug;

			if (console)
			{
				Handler consoleHandler = new ConsoleHandler();
				consoleHandler.setFormatter(new MyTextFormatter());
				LOGGER.addHandler(consoleHandler);
				
			}
			
			if (txt)
			{
				FileHandler txtHandler = new FileHandler("./Logger/Logger.txt", false);
				txtHandler.setFormatter(new MyTextFormatter());
				LOGGER.addHandler(txtHandler);
				
			}
			
			if (html)
			{
				FileHandler htmlHandler = new FileHandler("./Logger/Logger.html", false);
				htmlHandler.setFormatter(new MyHtmlFormatter());
				LOGGER.addHandler(htmlHandler);
			}
			return true;
		 } catch (Exception e) {
			 e.printStackTrace();
			 return false;
		 }
	}

	public static void close() {
		try {
			for (Handler handler : LOGGER.getHandlers()) 
				handler.close();
		} catch (Exception e) {
			System.out.println("Problem closing Logger handler: ");
			e.printStackTrace();
		}
	}	 
}
