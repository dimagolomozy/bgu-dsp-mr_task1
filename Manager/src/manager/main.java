package manager;
import Logger.MyLogger;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import common.Constants;

import java.io.IOException;

public class main
{

	public static void main(String[] args)
	{
		AWSCredentials awsCredentials;

		if (!MyLogger.init(false, true, true, false))
			return;

		// Load credentials
		try {
			awsCredentials = new PropertiesCredentials(Constants.awsCredentialInputStream);
		} catch (IOException | NullPointerException e) {
			MyLogger.LOGGER.severe("Error while opening credentials file");
			return;
		}

		MyLogger.LOGGER.info("Init Manager");
		Manager manager = new Manager(awsCredentials);

		// start manager
		MyLogger.LOGGER.warning("Manager start working...");
		manager.startWorking();
		MyLogger.LOGGER.warning("Manager exits");
		manager.cleanUp();

		MyLogger.close();
	}

}







































