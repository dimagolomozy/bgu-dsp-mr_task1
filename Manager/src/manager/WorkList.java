package manager;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class WorkList extends HashMap<String, HashMap<String, Task>>
{
    public WorkList() { }

    public void putTask(Task task)
    {
        if (super.containsKey(task.getClientId()))
        {
            super.get(task.getClientId()).put(task.getWorkId(), task);
            return;
        }

        HashMap<String, Task> tmpHash = new HashMap<>();
        tmpHash.put(task.getWorkId(), task);
        super.put(task.getClientId(), tmpHash);
    }

    public boolean containsTask(String clientId, String workId)
    {
        if (super.containsKey(clientId))
            if (super.get(clientId).containsKey(workId))
                return true;

        return false;
    }

    public Task getTask(String clientId, String workId)
    {
        return super.get(clientId).get(workId);
    }

    public void removeTask(Task task)
    {
        String clientId = task.getClientId();
        super.get(clientId).remove(task.getWorkId());

        if (super.get(clientId).isEmpty())
            super.remove(clientId);
    }
}
