package manager;

import Logger.MyLogger;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.sqs.model.Message;
import common.*;
import common.MyMessage.*;
import common.Wrappers.*;

import java.io.*;
import java.util.List;


public class Manager
{
    private final AWSCredentials awsCredentials;
    private final SQSWrapper sqs;
    private final S3Wrapper s3;
    private final EC2Wrapper ec2;
    private final String sqsManagerInputQueue;
    private final String sqsWorkersInputQueue;
    private final WorkList clientWorkList;
    private boolean shutdown;
    private boolean terminate;
    private int runningWorkers;
    // termination confirmation data:
    private String terminationFromClient;
    private String terminationWorkId;
    private String terminationReturnQueue;

    public Manager(AWSCredentials awsCredentials)
    {
        this.clientWorkList = new WorkList();
        this.shutdown = false;
        this.terminate = false;
        this.awsCredentials = awsCredentials;

        // connect to SQS
        MyLogger.LOGGER.info("Connecting to SQS");
        this.sqs = new SQSWrapper(awsCredentials);

        // connect to S3
        MyLogger.LOGGER.info("Connecting to S3");
        this.s3 = new S3Wrapper(awsCredentials);

        // connect to EC2
        MyLogger.LOGGER.info("Connecting to EC2");
        this.ec2 = new EC2Wrapper(awsCredentials);

        // connect to queues
        MyLogger.LOGGER.info("Connecting to Manager input queue: " + Constants.queueManagerInput);
        this.sqsManagerInputQueue = sqs.getQueueUrl(Constants.queueManagerInput);
        MyLogger.LOGGER.info("Creating Workers input queue: " + Constants.queueWorkersInput);
        this.sqsWorkersInputQueue = sqs.createQueue(Constants.queueWorkersInput);
    }

    public void startWorking()
    {
        while (!shutdown)
        {
            try
            {
                // receive messages from the input queue, 10 max
                List<Message> messages = sqs.receiveMessageFromQueue(sqsManagerInputQueue, 10);

                // run on all the messages
                for (Message message : messages)
                {
                    MyLogger.LOGGER.info(">> Got new message");
                    MyMessage msg = MyMessage.getMessage(message.getBody());
                    switch (msg.getType())
                    {
                        case MyMessage.messageNewTask:
                            if (!terminate)
                                handleNewTaskMessage((MyMessageNewTask) msg);
                            break;
                        case MyMessage.messageWorkResult:
                            handleWorkResultMessage((MyMessageWorkResult) msg);
                            break;
                        case MyMessage.messageTerminate:
                            handleTerminateMessage((MyMessageTerminate)msg);
                            break;
                        default:
                            MyLogger.LOGGER.warning("Got unknown/unsupported message type. wtf?");
                            continue;
                    }
                    // the message was handled, delete it from the queue.
                    sqs.deleteMessageFromQueue(sqsManagerInputQueue, message);
                }
            }
            catch (AmazonServiceException ase)
            {
                MyLogger.LOGGER.severe(SQSWrapper.getAmazonServiceException(ase));
            }
            catch (AmazonClientException ace)
            {
                MyLogger.LOGGER.severe(SQSWrapper.getAmazonClientException(ace));
            }
        }

    }

    private void handleWorkResultMessage(MyMessageWorkResult msgResult)
    {
        MyLogger.LOGGER.info(">> Handling result message");

        // Make sure the result message is valid and the task exists
        if (!clientWorkList.containsTask(msgResult.getClientId(), msgResult.getWorkId()))
        {
            MyLogger.LOGGER.warning("Task does not exists in this manager");
            return;
        }

        // Get the work's Task object
        Task task = clientWorkList.getTask(msgResult.getClientId(), msgResult.getWorkId());

        // Add the result object to the Task
        MyLogger.LOGGER.info("... adding result to task: " + task.getWorkId());
        task.addResult(msgResult);

        MyLogger.LOGGER.info("... completed: " + task.getSizeofResults() + " / " + task.getSizeOfPaths());
        // If task done - process its completion
        if (task.isWorkDone()) {
            MyLogger.LOGGER.info(">> work is done!");
            processCompletedTask(task);
        }
    }

    private void processCompletedTask(Task task)
    {
        MyLogger.LOGGER.info(">> Completed a task");

        // Create a textual summary file
        MyLogger.LOGGER.info("... creating summary file");
        File summaryFile = task.createSummary();
        String summaryFileName = summaryFile.getName();

        // Upload summary file to S3
        MyLogger.LOGGER.info("... uploading summary file to S3 bucket: " + Constants.bucketName);
        String fileS3Path = s3.uploadFileToBucket(Constants.bucketName, summaryFileName, summaryFile);
        summaryFile.delete();

        // Send TaskCompletion message back to client
        MyLogger.LOGGER.info("... sending message Task Completed to client");
        MyMessageTaskCompleted messageResult = new MyMessageTaskCompleted(task.getWorkId(), task.getClientId(), task.getFilePath(), fileS3Path);
        sqs.sendMessageToQueue(task.getClientInputQueue(), messageResult);

        // Remove work list
        clientWorkList.removeTask(task);
    }

    /**
     * Handle for receiving a work message from the client.
     * Downloads the file from S3
     * Create a new work from the inputFile
     * Create workers if needed and sends the work to them
     * @param msgWork the given work common.MyMessage
     */
    private void handleNewTaskMessage(MyMessageNewTask msgWork)
    {
        MyLogger.LOGGER.info(">> Handling NEW TASK message");
        MyLogger.LOGGER.info("Got task message from client: " + msgWork.getClientId());

        if (clientWorkList.containsTask(msgWork.getClientId(), msgWork.getWorkId()))
        {
            MyLogger.LOGGER.warning("Got duplicated task... ignoring");
            return;
        }

        // Get the file from S3 bucket
        MyLogger.LOGGER.info(String.format("... Downloading file: %s from S3", msgWork.getPath()));
        S3Object object = s3.getObjectFromBucket(Constants.bucketName, msgWork.getPath());

        // create a new task and init it
        MyLogger.LOGGER.info("... Creating task: " + msgWork.getWorkId());
        Task task = new Task(msgWork, object.getObjectContent());

        // save the task in the list
        clientWorkList.putTask(task);

        // start workers with the needed amount
        initWorkers(task.requiredAmountOfWorkers());

        // send the task to workers
        sendWorkToWorkers(task);
    }

    /**
     * Runs over all the paths and send each of them as a messageWork to the workersInputQueue
     * @param task the task to send to workers
     */
    private void sendWorkToWorkers(Task task)
    {
        MyLogger.LOGGER.info(String.format("... Sending task workId: %s to workers...", task.getWorkId()));
        for (String path : task.getInputPaths())
        {
            MyMessageWork messageWork = new MyMessageWork(task.getWorkId(), task.getClientId(), path, task.getClientInputQueue());
            //System.out.println("Sending message: " + messageWork);
            sqs.sendMessageToQueue(sqsWorkersInputQueue, messageWork.toString());
        }
        MyLogger.LOGGER.info("Sending task to workers... DONE");
    }


    private void initWorkers(int requiredAmountOfWorkers)
    {
        int instancesNumberToStart = requiredAmountOfWorkers - runningWorkers;

        if (instancesNumberToStart > 0)
        {
            MyLogger.LOGGER.info(String.format("... Creating %d Instances...", instancesNumberToStart));

            if (!Constants.isLocal)
            {
                List<Instance> instances = ec2.createInstances(instancesNumberToStart, Constants.sshKeyName, getWorkerBootstrap());
                ec2.tagInstances(instances, Constants.workerRoleTag);
            }

            runningWorkers += instancesNumberToStart;
        }
    }

    private String getWorkerBootstrap()
    {
        return "#!/bin/sh\n" +
                "export LC_ALL=C\n" +
                "export BASE_DIR=/tmp/worker/ \n" +
                "export AWS_ACCESS_KEY_ID=" + awsCredentials.getAWSAccessKeyId() + "\n" +
                "export AWS_SECRET_ACCESS_KEY=" + awsCredentials.getAWSSecretKey() + "\n" +
                "export AWS_DEFAULT_REGION=us-east-1\n" +
                "\n" +
                "mkdir -p $BASE_DIR\n" +
                "cd $BASE_DIR\n" +
                "wget http://sdk-for-java.amazonwebservices.com/latest/aws-java-sdk.zip\n" +
                "unzip aws-java-sdk.zip\n" +
                "mv aws-java-sdk-*/ aws-java-sdk\n" +
                "cd aws-java-sdk/lib/ \n" +
                "\n" +
                "aws s3 cp " + Constants.binaryWorker + " . \n" +
                "java -jar Worker.jar";
    }

    private void handleTerminateMessage(MyMessageTerminate msgTerminate)
    {
        MyLogger.LOGGER.info(">> Handling TERMINATE message");
        // If got a message from Client
        if (!terminate && !msgTerminate.isWorker())
        {
            MyLogger.LOGGER.info(">> ... got a new terminate from a CLIENT");
            terminationFromClient = msgTerminate.getClientId();
            terminationWorkId = msgTerminate.getWorkId();
            terminationReturnQueue = msgTerminate.getReturnQueue();
            terminate = true;
            MyLogger.LOGGER.info("... sending termination message to workers");
            for (int i = 0; i < runningWorkers; i++)
            {
                MyMessageTerminate messageTerminate = new MyMessageTerminate(msgTerminate.getWorkId(), msgTerminate.getClientId(), sqsManagerInputQueue, false);
                sqs.sendMessageToQueue(sqsWorkersInputQueue, messageTerminate);
            }
            return;
        }

        // Else - either not terminated or a message from worker is here
        if (msgTerminate.isWorker())
            runningWorkers--;

        if (runningWorkers == 0)
        {
            shutdown = true;
            MyLogger.LOGGER.info(".. all workers are down. sending termination message to client");
            MyMessageTerminate messageTerminate = new MyMessageTerminate(terminationWorkId, terminationFromClient, " ", false);
            sqs.sendMessageToQueue(terminationReturnQueue, messageTerminate);
        }
    }

    public void cleanUp()
    {
        MyLogger.LOGGER.warning("Clean up...");
        if (!Constants.isLocal)
            ec2.terminateInstances(this.ec2.getInstancesByTag(Constants.workerRoleTag));
        // close workers input queue
        sqs.deleteQueue(sqsWorkersInputQueue);
        // close manager input queue
        sqs.deleteQueue(sqsManagerInputQueue);
    }
}
