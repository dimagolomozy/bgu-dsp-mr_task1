package manager;

import Logger.MyLogger;
import common.MyMessage.MyMessageNewTask;
import common.MyMessage.MyMessageWorkResult;

import java.io.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;


public class Task
{
    private final String workId;
    private final String filePath;
    private final String clientId;
    private final int messagesPerWorker;
    private final HashMap<String, MyMessageWorkResult> taskData;
    private final String clientInputQueue;  // the 'result' output message
    private final int pathsAmount;
    private int resultsAmount;

    public Task(MyMessageNewTask msgNewTask, InputStream inputFile)
    {
        this.workId = msgNewTask.getWorkId();
        this.filePath = msgNewTask.getPath();
        this.clientId = msgNewTask.getClientId();
        this.messagesPerWorker = msgNewTask.getMessagesPerWorker();
        this.clientInputQueue = msgNewTask.getReturnQueue();
        this.taskData = new HashMap<>();
        parseInputFile(inputFile);
        this.pathsAmount = taskData.size();
        this.resultsAmount = 0;
    }

    private void parseInputFile(InputStream inputFile)
    {
        try
        {
            BufferedReader readed = new BufferedReader(new InputStreamReader(inputFile));
            String line = null;
            while ((line = readed.readLine()) != null)
                taskData.put(line, null);

            readed.close();
        }
        catch (IOException ex)
        {
            MyLogger.LOGGER.warning(String.format("Task workId: %s encountered a problem", workId));
        }
    }

    public String getClientId() {
        return clientId;
    }

    public int requiredAmountOfWorkers() {
        MyLogger.LOGGER.warning(String.format("We have %d files and n=%d = so we need %d workers", pathsAmount, messagesPerWorker, (int)Math.ceil(((double)pathsAmount) / messagesPerWorker)));
        return (int)Math.ceil(((double)pathsAmount) / messagesPerWorker);
    }

    public int getSizeofResults()
    {
        return resultsAmount;
    }

    public int getSizeOfPaths() { return pathsAmount; }

    public HashMap<String, MyMessageWorkResult> getTaskData()
    {
        return taskData;
    }

    public String getWorkId()
    {
        return workId;
    }

    public String getFilePath()
    {
        return filePath;
    }

    public String getClientInputQueue() { return  clientInputQueue; }

    public boolean isWorkDone()
    {
        return pathsAmount == resultsAmount;
    }

    public File createSummary()
    {
        FileWriter fw = null;
        BufferedWriter bw = null;
        File file = null;
        try
        {
            String path = "./summary_" + workId + ".txt";
            file = new File(path);

            fw = new FileWriter(file.getAbsoluteFile());
            bw = new BufferedWriter(fw);
            writeSummaryContent(bw);
        }
        catch (IOException ex)
        {
            MyLogger.LOGGER.warning(String.format("Could not create summary file workId: %s", workId));
        }
        finally
        {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ignored) {}
        }
        return file;
    }

    private void writeSummaryContent(BufferedWriter bw) throws IOException
    {
        for (MyMessageWorkResult msg : taskData.values())
        {
            bw.write(msg.getOldPath() + ";");
            bw.write(msg.getSuccess().toString() + ";");
            bw.write(msg.getNewPath() + ";\n");
        }
    }

    public void addResult(MyMessageWorkResult msgResult)
    {
        if (isWorkDone())
            MyLogger.LOGGER.warning(String.format("Try to add result message: %s to a done work: %s", msgResult.getWorkId(), workId));
        else
        {
            if (taskData.get(msgResult.getOldPath()) == null)
            {
                taskData.put(msgResult.getOldPath(), msgResult);
                resultsAmount++;
            }
        }
    }

    public Set<String> getInputPaths()
    {
        return taskData.keySet();
    }
}
