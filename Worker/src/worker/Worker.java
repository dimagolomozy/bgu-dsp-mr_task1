package worker;

import Logger.MyLogger;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.sqs.model.Message;
import common.Constants;
import common.MyMessage.MyMessage;
import common.MyMessage.MyMessageWorkResult;
import common.MyMessage.MyMessageTerminate;
import common.MyMessage.MyMessageWork;
import common.Wrappers.S3Wrapper;
import common.Wrappers.SQSWrapper;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class Worker
{
    private final S3Wrapper s3;
    private final SQSWrapper sqs;
    private final String sqsInputQueue;
    private final String sqsOutputQueue;
    private final String workerId;
    private boolean shutdown;

    // statistics helpers
    private final Date startTime;
    private long totalRuntime;
    private int totalUrlHandles;
    private final LinkedList<String> successfulUrls;
    private final HashMap<String, Exception> failedUrls;


    // Constructor
    public Worker(AWSCredentials awsCredentials)
    {
        // connect to SQS
        MyLogger.LOGGER.info("Connecting to SQS");
        sqs = new SQSWrapper(awsCredentials);

        // connect to S3
        MyLogger.LOGGER.info("Connecting to S3");
        s3 = new S3Wrapper(awsCredentials);

        // connect to queues
        MyLogger.LOGGER.info("Connecting to input queue: " + Constants.queueWorkersInput);
        this.sqsInputQueue = sqs.getQueueUrl(Constants.queueWorkersInput);
        MyLogger.LOGGER.info("Connecting to output queue: " + Constants.queueManagerInput);
        this.sqsOutputQueue = sqs.getQueueUrl(Constants.queueManagerInput);

        // init shutdown
        this.shutdown = false;

        // init statistics helpers
        this.startTime = new Date();
        this.totalRuntime = 0;
        this.totalUrlHandles = 0;
        this.successfulUrls = new LinkedList<>();
        this.failedUrls = new HashMap<>();
        this.workerId = "worker_" + UUID.randomUUID().toString();
    }

    public void startWorking()
    {
        while (!shutdown)
        {
            try
            {
                // receive messages from the input queue, 1 max
                List<Message> messages = sqs.receiveMessageFromQueue(sqsInputQueue, 1);

                // run on all the messages
                for (Message message : messages)
                {
                    if (!shutdown)
                    {
                        MyLogger.LOGGER.info(">> Got new message");
                        MyMessage msg = MyMessage.getMessage(message.getBody());
                        switch (msg.getType())
                        {
                            case MyMessage.messageWork:
                                handleWorkMessage((MyMessageWork)msg);
                                break;
                            case MyMessage.messageTerminate:
                                handleTerminateMessage((MyMessageTerminate)msg);
                                break;
                            default:
                                MyLogger.LOGGER.warning("Got unknown/unsupported message type. wtf?");
                                continue;
                        }
                        // the message was handled, delete it from the queue.
                        sqs.deleteMessageFromQueue(sqsInputQueue, message);
                    }
                }
            }
            catch (AmazonServiceException ase)
            {
                MyLogger.LOGGER.severe(SQSWrapper.getAmazonServiceException(ase));
            }
            catch (AmazonClientException ace)
            {
                MyLogger.LOGGER.severe(SQSWrapper.getAmazonClientException(ace));
            }
            catch (MalformedURLException exUrl)
            {
                StringBuilder error = new StringBuilder();
                error.append("Error in handling image url \n");
                error.append(exUrl.toString());
                MyLogger.LOGGER.warning(error.toString());
            }
            catch (IOException exReadImage)
            {
                StringBuilder error = new StringBuilder();
                error.append("Error in reading image url: \n");
                error.append(exReadImage.toString());
                MyLogger.LOGGER.warning(error.toString());
            }
        }
    }

    private void handleWorkMessage(MyMessageWork msg) throws IOException
    {
        long start = new Date().getTime();

        MyLogger.LOGGER.info(">> Handling new work message");

        try
        {
            String path = msg.getPath();

            // Resize image
            File file = handleImage(path);
            String fileName = file.getName();

            // Upload to S3 (and make it public)
            MyLogger.LOGGER.info(".. saving resized image to output bucket: " + Constants.bucketOutputFolder + msg.getWorkId() + "/" + fileName);
            String fileS3Path = s3.uploadFileToBucket(Constants.bucketName, Constants.bucketOutputFolder + msg.getWorkId() + "/" + fileName, file, true);

            // Send result message back to manager
            MyLogger.LOGGER.info(".. sending result message to manager");
            MyMessageWorkResult messageResult = new MyMessageWorkResult(msg.getWorkId(), msg.getClientId(), msg.getPath(), fileS3Path, true);
            sqs.sendMessageToQueue(sqsOutputQueue, messageResult);
            file.delete();
            successfulUrls.add(msg.getPath());
        }
        catch (Exception ex)
        {
            MyLogger.LOGGER.warning("!! Invalid file path");

            try {
                // Send message indicates error
                MyLogger.LOGGER.info(".. sending result message (with error) to manager");
                MyMessageWorkResult messageResult = new MyMessageWorkResult(msg.getWorkId(), msg.getClientId(), msg.getPath(), ex.toString(), false);
                sqs.sendMessageToQueue(sqsOutputQueue, messageResult);
            }
            catch (Exception ex2) {
                // do nothing. an error while writing to queue. we don't want to crash
            }

            failedUrls.put(msg.getPath(), ex);
        }

        long end = new Date().getTime();
        totalRuntime += (end - start);
        totalUrlHandles++;
    }

    private File handleImage(String path) throws IOException
    {
        // load image from the internet
        MyLogger.LOGGER.info(".. downloading image from path: " + path);
        URL url = new URL(path);
        BufferedImage inputImage = ImageIO.read(url);

        // get data
        double width = Constants.imageScale;
        double height = Constants.imageScale;
        int imageType = inputImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : inputImage.getType();
        String extension = path.substring(path.lastIndexOf('.') + 1, path.length());

        // resize image
        MyLogger.LOGGER.info(".. resizing image");
        BufferedImage outputImage = new BufferedImage((int)width, (int)height, imageType);
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, (int) width, (int) height, null);
        g2d.dispose();

        // save to disk
        String newPath = "./resized_" + UUID.randomUUID();
        MyLogger.LOGGER.info(".. saving image to disk: " + newPath);
        File file = new File(newPath);
        ImageIO.write(outputImage, extension, file);

        return file;
    }


    private void handleTerminateMessage(MyMessageTerminate msg)
    {
        MyLogger.LOGGER.info(">> Handling TERMINATE message id: " + msg.getWorkId());
        shutdown = true;

        handleStatistics();

        // create terminate message and send to queue
        MyLogger.LOGGER.info(".. sending terminated message to manager");
        MyMessageTerminate messageTerminate = new MyMessageTerminate(msg.getWorkId(), msg.getClientId(), " ", true);
        sqs.sendMessageToQueue(sqsOutputQueue, messageTerminate);
    }

    private void handleStatistics()
    {
        FileWriter fw = null;
        BufferedWriter bw = null;
        File file = null;
        try
        {
            String path = "./statistics_" + workerId + ".txt";
            MyLogger.LOGGER.info(">> Creating statistics file: " + path);
            file = new File(path);

            fw = new FileWriter(file.getAbsoluteFile());
            bw = new BufferedWriter(fw);
            bw.write(createStatisticsContent());
            bw.close();
            fw.close();

            MyLogger.LOGGER.info(".. saving statistics file in s3 bucket: " + Constants.bucketName);
            s3.uploadFileToBucket(Constants.bucketName, file.getName(), file);

            // TODO: do we need to send it back to client? manager?
            file.delete();
        }
        catch (IOException ex)
        {
            MyLogger.LOGGER.warning(String.format("Could not create statistics file workId: %s", workerId));
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ignored) {}
        }
    }

    private String createStatisticsContent()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        StringBuilder state = new StringBuilder();
        state.append("Worker ID: " + workerId + "\n");

        state.append("Start time: " + dateFormat.format(startTime) + "\n");

        long avrgTime = totalRuntime / totalUrlHandles;
        state.append("Average run-time on singale url: " + avrgTime + " milliseconds\n");

        state.append("Successful urls:\n");
        for (String str : successfulUrls)
            state.append("URL: " + str + "\n");

        state.append("Failed urls:\n");
        for (Map.Entry entry : failedUrls.entrySet())
        {
            state.append("URL: " + entry.getKey().toString() + "\n");
            state.append("Exception: " + entry.getValue().toString() + "\n");
        }

        state.append("Finish time: " + dateFormat.format(new Date()));

        return state.toString();
    }
}
