package worker;
import Logger.MyLogger;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import common.Constants;

import java.io.IOException;

public class main
{

	public static void main(String[] args)
    {
        if (!MyLogger.init(false, true, true, false))
            return;

        AWSCredentials awsCredentials;

        // Load credentials
        try {
            awsCredentials = new PropertiesCredentials(Constants.awsCredentialInputStream);
        } catch (IOException | NullPointerException e) {
            MyLogger.LOGGER.severe("Error while opening credentials file");
            return;
        }

        MyLogger.LOGGER.info("Init Worker");
        Worker worker = new Worker(awsCredentials);

        // start the main working instance
        MyLogger.LOGGER.warning("Worker start working...");
        worker.startWorking();
        MyLogger.LOGGER.warning("Worker exits");

        MyLogger.close();
    }
}
