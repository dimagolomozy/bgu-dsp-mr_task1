package program;

import Logger.MyLogger;
import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.ec2.model.*;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.sqs.model.Message;
import common.Constants;
import common.MyMessage.*;
import common.Wrappers.EC2Wrapper;
import common.Wrappers.S3Wrapper;
import common.Wrappers.SQSWrapper;

import java.io.*;
import java.util.*;


public class Client {

    private final AWSCredentials credentials;
    private final EC2Wrapper ec2;
    private final S3Wrapper s3;
    private final SQSWrapper sqs;
    private final String clientId; // client unique workId

    private boolean initialized;
    private String queueOutputUrl;
    private String queueInputUrl;

    public class MoreThanOneRunningManagersException extends Exception { }

    public Client(AWSCredentials credentials) {
        this.credentials = credentials;
        this.clientId = UUID.randomUUID().toString();

        this.ec2 = new EC2Wrapper(this.credentials);
        this.s3  = new S3Wrapper(this.credentials);
        this.sqs = new SQSWrapper(this.credentials);

        this.initialized = false;
    }

    /**
     * Initialization of environment.
     * Creates whatever is missing
     * @throws MoreThanOneRunningManagersException
     */
    public void initialize() throws MoreThanOneRunningManagersException {
        if (initialized)
            return;

        MyLogger.LOGGER.info(">>> Initializing Client (or reusing)");

        MyLogger.LOGGER.info("# Creating bucket");
        s3.createBucket(Constants.bucketName);

        MyLogger.LOGGER.info("# Creating SQS Queue");
        queueOutputUrl = sqs.createQueue(Constants.queueManagerInput);
        queueInputUrl = sqs.createQueue(Constants.queueClientInput);

        MyLogger.LOGGER.info("# Creating Manager");
        if (!Constants.isLocal)
            createOrLoadManager();

        MyLogger.LOGGER.info("# ... done");
        initialized = true;
    }

    /**
     * Completely erases all environment.
     * Deletes input and output queue and the manager instance
    */
    public void cleanup()
    {
        MyLogger.LOGGER.warning(">>> Full cleaning up");
        if (!Constants.isLocal)
            this.ec2.terminateInstances(this.ec2.getInstancesByTag(Constants.managerRoleTag));
        closeInputQueue();
    }

    public void closeInputQueue()
    {
        try {
            this.sqs.deleteQueue(queueInputUrl);
        } catch (AmazonClientException e) { MyLogger.LOGGER.severe(e.toString()); }
    }

    public void handleNewFile(File inputFile, File outputFile, int n) throws IOException
    {
        String uniqueTaskId = UUID.randomUUID().toString();
        String fileUrl;

        MyLogger.LOGGER.info(">>> Handling a new file");
        MyLogger.LOGGER.info("...     allocated task id: " + uniqueTaskId);

        // 1. Upload file to bucket
        MyLogger.LOGGER.info("# Uploading file");
        fileUrl = s3.uploadFileToBucket(Constants.bucketName, Constants.bucketInputFolder + clientId, inputFile);
        MyLogger.LOGGER.info("# ... file was uploaded to: " + fileUrl);

        // 2. Send message to manager
        MyLogger.LOGGER.info("# Sending message to manager queue");
        sendWorkMessageToManager(uniqueTaskId, queueOutputUrl, n, fileUrl);

        // 3. Wait for task completion
        MyLogger.LOGGER.info("# Waiting for process completion");
        String summaryFilePath = waitForSummary(uniqueTaskId);

        // 4. Dump HTML File
        MyLogger.LOGGER.info("# Dumping summary file: " + summaryFilePath);
        dumpHtmlFile(summaryFilePath, outputFile);
    }

    private void dumpHtmlFile(String summaryFilePath, File outputFile) throws IOException
    {
        String head = "<html>" + "<head><title>Summary</title></head>" + "<body>";
        String tail = "</body>" + "</html>";
        String body = "";
        int counter_success = 0;
        int counter_failed = 0;

        // Download file
        S3Object object = s3.getObjectFromBucket(Constants.bucketName, summaryFilePath);
        InputStream content = object.getObjectContent();
        BufferedReader reader = new BufferedReader(new InputStreamReader(content));

        String line = null;
        while ((line = reader.readLine()) != null)
        {
            String[] result = line.split(";");
            if ("false".equals(result[1]))
            {
                counter_failed++;
                continue;
            }
            counter_success++;
            body += createHtmlImageLink(result[0], result[2]) + "\n";
        }
        MyLogger.LOGGER.info("Success links: " + counter_success + ", Failed links: " + counter_failed);
        createOutput(head + body + tail, outputFile);
    }

    private void createOutput(String content, File outputFile)
    {
        MyLogger.LOGGER.info("Creating html file: " + outputFile.getAbsolutePath());
        FileWriter fw = null;
        BufferedWriter bw = null;

        try
        {
            fw = new FileWriter(outputFile.getAbsoluteFile());
            bw = new BufferedWriter(fw);
            bw.write(content);
        }
        catch (IOException ex)
        {
            MyLogger.LOGGER.warning("Could not create output file");
        }
        finally
        {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException ignored) {}
        }
    }

    private String createHtmlImageLink(String oldImagePath, String newImagePath)
    {
        return "<a href=\"" + oldImagePath + "\">\n" +
                "<img src=\"https://s3.amazonaws.com/" + Constants.bucketName + "/" + newImagePath + "\" width=50 height=50>\n" +
                "</a>";
    }

    public void createOrLoadManager() throws MoreThanOneRunningManagersException {
        if (isManagerRunning()) {
            MyLogger.LOGGER.info("Manager running");
        }
        else {
            createManager();
        }
    }

    private void createManager() {

        MyLogger.LOGGER.info("Creating manager");

        List<Instance> instances = ec2.createInstances(1, Constants.sshKeyName, getManagerBootstrap());

        this.ec2.tagInstances(instances, Constants.managerRoleTag);
    }

    public void sendWorkMessageToManager(String uniqueTaskId, String queueUrl, int messagerPerWorker, String fileUrl)
    {
        MyMessageNewTask messageNewTask = new MyMessageNewTask(uniqueTaskId, clientId, fileUrl, messagerPerWorker, queueInputUrl);
        sqs.sendMessageToQueue(queueUrl, messageNewTask);
    }

    public String getManagerBootstrap() {
        return "#!/bin/sh\n" +
                "export LC_ALL=C\n" +
                "export BASE_DIR=/tmp/manager/ \n" +
                "export AWS_ACCESS_KEY_ID=" + credentials.getAWSAccessKeyId() + "\n" +
                "export AWS_SECRET_ACCESS_KEY=" + credentials.getAWSSecretKey() + "\n" +
                "export AWS_DEFAULT_REGION=us-east-1\n" +
                "\n" +
                "mkdir -p $BASE_DIR\n" +
                "cd $BASE_DIR\n" +
                "wget http://sdk-for-java.amazonwebservices.com/latest/aws-java-sdk.zip\n" +
                "unzip aws-java-sdk.zip\n" +
                "mv aws-java-sdk-*/ aws-java-sdk\n" +
                "cd aws-java-sdk/lib/ \n" +
                "\n" +
                "aws s3 cp " + Constants.binaryManager + " . \n" +
                "java -jar Manager.jar";
    }

    public Instance getManagerInstance() throws MoreThanOneRunningManagersException {
        List<Instance> instances = this.ec2.getInstancesByTag("manager");

        if (instances.size() == 0)
            return null;

        if (instances.size() != 1) {
            MyLogger.LOGGER.severe("More than one manager is running! this can't be");
            throw new MoreThanOneRunningManagersException();
        }

        return instances.get(0);
    }

    public boolean isManagerRunning() throws MoreThanOneRunningManagersException {
        return getManagerInstance() != null;
    }

    public String waitForSummary(String uniqueTaskId) {
        List<Message> messages = null;
        String resultFile = null;

        // Wait for messages in client's own queue
        try {
            while (true) {
                messages = sqs.receiveMessageFromQueue(queueInputUrl, 1);
                if (messages.size() != 0)
                    break;
            }
        }
        catch (AmazonClientException ace)
        {
            MyLogger.LOGGER.severe(SQSWrapper.getAmazonClientException(ace));
            return null;
        }

        // Run on "all" messages
        for (Message message : messages)
        {
            // convert to our own protocol
            MyMessage msg = MyMessage.getMessage(message.getBody());

            // Assert message type
            if (!Objects.equals(msg.getType(), MyMessage.messageTaskCompleted)) {
                MyLogger.LOGGER.warning("Unexpected message was sent to client 'results queue'");
                return null;
            }

            MyMessageTaskCompleted completionMsg = (MyMessageTaskCompleted)msg;

            // Assert client id
            if (!clientId.equals(completionMsg.getClientId())) {
                MyLogger.LOGGER.warning("Unexpected message received - client id is different (this msg wasn't for me! but this is a private queue");
                return null;
            }
            // Assert work id
            if (!uniqueTaskId.equals(completionMsg.getWorkId())) {
                MyLogger.LOGGER.warning("Unexpected message received - work id is different");
                return null;
            }

            resultFile = completionMsg.getResultsFile();

            // the message was handled, delete it from the queue.
            sqs.deleteMessageFromQueue(queueInputUrl, message);

            break;
        }
        return resultFile;
    }

    public void handleTerminateMessage()
    {
        MyLogger.LOGGER.info(">>> Handling TERMINATE message");
        // send termination message to manager
        MyMessageTerminate messageTerminate = new MyMessageTerminate(UUID.randomUUID().toString(), clientId, queueInputUrl, false);
        sqs.sendMessageToQueue(queueOutputUrl, messageTerminate);
        MyLogger.LOGGER.info(String.format("# Sending termination message id: %s to manager", messageTerminate.getWorkId()));

        // Wait for confirmation
        waitForTerminateConfirmation(messageTerminate.getWorkId());
    }

    public void waitForTerminateConfirmation(String terminationId)
    {
        List<Message> messages = null;

        // Wait for messages in client's own queue
        try {
            while (true) {
                messages = sqs.receiveMessageFromQueue(queueInputUrl, 1);
                if (messages.size() != 0)
                    break;
            }
        }
        catch (AmazonClientException ace)
        {
            MyLogger.LOGGER.severe(SQSWrapper.getAmazonClientException(ace));
            return;
        }

        // Run on "all" messages
        for (Message message : messages)
        {
            // convert to our own protocol
            MyMessage msg = MyMessage.getMessage(message.getBody());

            // Assert message type
            if (!Objects.equals(msg.getType(), MyMessage.messageTerminate)) {
                MyLogger.LOGGER.warning("Unexpected message was sent to client 'results queue'");
                return;
            }

            MyMessageTerminate terminateMessage = (MyMessageTerminate)msg;

            // Assert client id
            if (!clientId.equals(terminateMessage.getClientId())) {
                MyLogger.LOGGER.warning("Unexpected message received - client id is different (this msg wasn't for me! but this is a private queue");
                return;
            }
            // Assert work id
            if (!terminationId.equals(terminateMessage.getWorkId())) {
                MyLogger.LOGGER.warning("Unexpected message received - terminate id is different");
                return;
            }

            // the message was handled, delete it from the queue.
            sqs.deleteMessageFromQueue(queueInputUrl, message);

            break;
        }
    }

}
