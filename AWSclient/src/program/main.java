package program;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import Logger.MyLogger;
import common.Constants;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class main {

    /**
     * main function - starting point
     * @param args :
     * 		args[0] = The name of the input file
     * 		args[1] = The name of the output file
     * 		args[2] = Workers - files ratio (how many urls per worker)
     * 	    args[3] = Terminate (optional)
     */
    public static void main(String[] args) throws IOException {
        AWSCredentials credentials;

        // Init logging system
        if (!MyLogger.init(false, true, false, false))
            return;

        // Check arguments amount
        if ((args.length != 3) && (args.length != 4)) {
            MyLogger.LOGGER.severe("Invalid usage: <inputFileName> <outputFileName> <n> [terminate]");
            return;
        }

        // Load arguments
        File inputFile = new File(args[0]);
        File outputFile = new File(args[1]);
        int n = Integer.valueOf(args[2]);
        Boolean terminate = (args.length == 4 && Boolean.parseBoolean(args[3]));


        // Make sure files exists
        if (!inputFile.exists()) {
            MyLogger.LOGGER.severe("Input file does not exists;");
            return;
        }

        // Load credentials
        try {
            credentials = new PropertiesCredentials(Constants.awsCredentialInputStream);
        } catch (IOException | NullPointerException e) {
            MyLogger.LOGGER.severe("Error while opening credentials file");
            return;
        }

        Client client = new Client(credentials);
        // get the starting time
        long start = new Date().getTime();

        try {
            client.initialize();
        } catch (Client.MoreThanOneRunningManagersException e) {
            e.printStackTrace();
        }

        // send input file to manager and wait for summary
        client.handleNewFile(inputFile, outputFile, n);

        // sends termination message to managers and waits for confirmation
        if (terminate)
        {
            client.handleTerminateMessage();
            // cleans up the env
            client.cleanup();
        }
        else
            client.closeInputQueue();


        // get the end time
        long end = new Date().getTime();
        long runtime = (end - start);
        DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
        MyLogger.LOGGER.info("Start time: " + dateFormat.format(start));
        MyLogger.LOGGER.info("End time: " + dateFormat.format(end));
        MyLogger.LOGGER.info("Seconds: " + (runtime / 1000));
        MyLogger.LOGGER.info("Milliseconds: " + runtime);

        MyLogger.close();
    }

}
