# **Distributed System Programming: Scale Out with Cloud Computing and Map-Reduce - 2015/Spring** #

Instructions:
1. Must have aws-java-sdk-1.9.33.jar in the directory of the jar!
2. Open terminal and run the client with:
        > java -jar AWSClient.jar inputFileName outputFileName n
   or, if you want to terminate the manager:
        > java -jar AWSClient.jar inputFileName outputFileName n terminate

inputFileName is the name of the input file.
outputFileName is the name of the output file.
n is: workers - files ratio (how many urls per worker).
terminate is a flag for shutting down the manager.

Information:
1. Image id: ami-146e2a7c
2. Instance type: t2.micro
3. n = 100
4. TIMES:
        Start time: 06:09:35
        End time: 06:24:15
        Seconds: 880
        Milliseconds: 880261

